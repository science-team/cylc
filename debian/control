Source: cylc-flow
Section: utils
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Alastair McKinstry <mckinstry@debian.org>
Build-Conflicts: cylc
Build-Depends: debhelper-compat (= 13), 
 dh-sequence-python3,  
 python3-setuptools,
 python3-all-dev, 
 less,
 texlive-latex-extra, 
 texlive-fonts-recommended,  
 lmodern, 
 tex4ht, 
 imagemagick, 
 sqlite3,
 tex-gyre
Standards-Version: 4.7.0
Homepage: https://cylc.github.io/cylc
Vcs-Browser: https://salsa.debian.org/science-team/cylc.git
Vcs-Git: https://salsa.debian.org/science-team/cylc.git

Package: cylc-flow
Depends: python3, ${misc:Depends}, 
 sqlite3, 
 python3-cylc, 
 python3-isodatetime,
 at, perl, python3-ansimarkup
Architecture: all
Provides: cylc
Breaks: cylc
Replaces: cylc
Description: Workflow scheduler 
 Cylc ("silk") is a suite engine and meta-scheduler that specializes
 in suites of cycling tasks for weather forecasting, climate modeling,
 and related processing (it can also be used for one-off workflows
 of non-cycling tasks, which is a simpler problem).

Package: python3-cylc
Section: python
Architecture:  all
Depends: python3, ${python3:Depends}, ${misc:Depends}, 
 fonts-font-awesome,
 python3-josepy, 
 python3-colorama, 
 python3-isodatetime,
 python3-zmq,
 libjs-jquery, libjs-prettify,
 libjs-bootstrap, libjs-moment
Description: Python3 libraries for cylc workflow scheduler
 Cylc ("silk") is a suite engine and meta-scheduler that specializes
 in suites of cycling tasks for weather forecasting, climate modeling,
 and related processing (it can also be used for one-off workflows
 of non-cycling tasks, which is a simpler problem).
 .
 This package contains python library code used by cylc.

