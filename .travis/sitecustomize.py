<<<<<<< HEAD:.travis/sitecustomize.py
#!/usr/bin/env python3

# THIS FILE IS PART OF THE CYLC SUITE ENGINE.
# Copyright (C) 2008-2019 NIWA & British Crown (Met Office) & Contributors.
=======
# THIS FILE IS PART OF THE CYLC WORKFLOW ENGINE.
# Copyright (C) NIWA & British Crown (Met Office) & Contributors.
>>>>>>> upstream/8.0_b2:cylc/flow/main_loop/prune_flow_labels.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Prune excess common flow labels."""

<<<<<<< HEAD:.travis/sitecustomize.py
# This file is used by Travis-CI to start the coverage process.
# In order to make Cylc and Python aware of it, we export PYTHONPATH when
# running the tests.

import coverage
coverage.process_startup()
=======
from cylc.flow.main_loop import periodic


@periodic
async def prune_flow_labels(scheduler, _):
    """Prune flow labels."""
    scheduler.pool.prune_flow_labels()
>>>>>>> upstream/8.0_b2:cylc/flow/main_loop/prune_flow_labels.py
